const path = require('path');
const express =require('express');
const { v4: uuidv4 } = require('uuid');


const app = express();

const PORT = process.env.PORT || 8000;

app.use((req,res,next) =>{
        if (req.path=="/"){
                res.sendStatus(400); 
        }
        next();
})

app.get('/html',(req,res)=>{
        res.sendFile(path.join(__dirname,'public','/index.html'),function(err){
                if(err){
                        res.sendStatus(500).end();
                }
        });    
})

app.get('/json',(req,res)=>{
        res.sendFile(path.join(__dirname,'public','/slideshow.json'),function(err){
                if(err){
                        res.sendStatus(500).end();
                }
        });      
})

app.get('/uuid',(req,res)=>{
        res.status(200);
        res.json({
                uuid: uuidv4()
        }).end();
})

app.get('/status/:code',(req,res)=>{
       try{
                res.sendStatus(req.params.code).end();
       }catch(err){
                res.sendStatus(400).end();
       }
})

app.get('/delay/:delay_in_second',async (req,res)=>{
        
        const delay =parseInt(req.params.delay_in_second);

        if ( Number.isInteger(delay) && delay > 0 ){
                setTimeout(()=>{
                        res.status(200);
                        res.json({
                                'msg': "delay"
                        }).end();
                },delay*1000);
                
        }else{
                res.sendStatus(400).end();
        }
})

app.listen(PORT,()=>{
        console.log(`listing on port ${PORT}`);
})
